# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
import jieba.analyse as jb_an
from zp.base_model import DB_Util,DD,Zwmc,Gsmc,Zwlb,ZwlbBig,Gshy,Gsxz,Gzjy,Xl,Gsgm,List,ListOld,GwzzFenCi,RzyqFenCi,Flxx,engine
import time
import redis
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

#数据清洗
class DataCleanPipeline(object):
    def process_item(self,item,spider):
        #去除职位名称和公司名称的数据的两端空格
        item['zwmc']=item['zwmc'].strip()
        item['gzdd']=item['gzdd'].split('-')[0].split('/')[0].strip('市省')
        item['gsmc'] = item['gsmc'].strip()
        if item['zwmc']=='' or item['zdxl']=='' or item['gzdd']=='' or item['gsmc']=='':
            #如果数据为空，那么返回DropItem,传递的文本信息任意
            raise DropItem('The position has failed')
        if item['gsxz']=='':
            item['gsxz']='其它'
        if item['min_zwyx']=='' or item['max_zwyx']=='':
            item['min_zwyx']=item['max_zwyx']=0
        if item['gzjy']=='':
            item['gzjy']='不限' 
        if item['zprs']=='若干':
            item['zprs']=0
        if item['gsgm']=='':
            item['gsgm']='其他'
        return item

#发布时间转换为正确的时间格式
class DataTimePipeline(object):
    def process_item(self,item,spider):
        # 判断是否为空
        if item['fbrq']=='' or '0002-01-01' in item['fbrq']:
            ##修改日期为今天的日期,使用time完成操作
            now_timetmp=time.time()  #当前的时间戳
            last_day_timetmp=now_timetmp
            last_day_tupel=time.localtime(last_day_timetmp)#将时间戳转换为时间元祖
            item['fbrq']=time.strftime('%Y-%m-%d %H:%M:%S',last_day_tupel)#时间元祖格式化为规定的时间字符串
        else:
            item['fbrq']=item['fbrq'][:10]+' '+item['fbrq'][10:]
        return item

#数据去重(针对的一次爬虫执行过程中数据重复的处理)
class DuplicatesPipeline(object):
    #scrapy会自动过滤已经走过的链接，但是很多情况，不同链接，可能存在同样的数据
    #判断职位和公司是否已经抓取过
    def __init__(self):
        pool = redis.ConnectionPool(host='127.0.0.1',password='FanTan879425', port=6379)
        self.conn = redis.Redis(connection_pool=pool)
    def process_item(self,item,spider):
        if item['zwmc']=='' or item['gsmc']=='' or item['gsmc']==None or item['zwlb']=='' or self.conn.sismember('zwmc_gsmc_zwlb',item['zwmc']+'_'+item['gsmc']+'_'+item['zwlb']):
            #如果存在这样的数据组合，那么表示职位数据已经存在
            #返回异常
            raise DropItem('Duplicate item found %s'%item)
        else:
            #数据组合添加到集合内
            self.conn.sadd('zwmc_gsmc_zwlb',item['zwmc']+'_'+item['gsmc']+'_'+item['zwlb'])
            return item


class MysqlSavePipeline(object):
    def open_spider(self, spider):
        DB_Util.init_db()  # 表不存在时候,初始化表结构

    def process_item(self, item, spider):
        if not item['href']:
            raise DropItem('item info_id is null.{0}'.format(item))
        else:
            session = DB_Util.get_session()

            # 地点
            zp_dd=session.query(DD).filter_by(dd_name=item['gzdd']).first()
            if zp_dd:
                dd_id = zp_dd.id
            else:
                dd=DD()
                dd.dd_name=item['gzdd']
                session.add(dd)
                session.commit()
                session.flush()
                session.refresh(dd)
                dd_id=dd.id

            #职位名称
            zp_zwmc = session.query(Zwmc).filter_by(zwmc_name=item['zwmc']).first()
            if zp_zwmc:
                zwmc_id = zp_zwmc.id
            else:
                zwmc = Zwmc()
                zwmc.zwmc_name = item['zwmc']
                session.add(zwmc)
                session.flush()
                session.refresh(zwmc)
                zwmc_id = zwmc.id
            # 公司名称
            zp_gsmc = session.query(Gsmc).filter_by(gsmc_name=item['gsmc']).first()
            if zp_gsmc:
                gsmc_id = zp_gsmc.id
            else:
                gsmc = Gsmc()
                gsmc.gsmc_name = item['gsmc']
                session.add(gsmc)
                session.flush()
                session.refresh(gsmc)
                gsmc_id = gsmc.id

            # 职位大分类
            print(item)
            zp_zwlb_big = session.query(ZwlbBig).filter_by(zwlb_big_name=item['zwlb_big']).first()
            if zp_zwlb_big:
                zwlb_big_id = zp_zwlb_big.id
            else:
                zwlb_big = ZwlbBig(zwlb_big_name=item['zwlb_big'])
                session.add(zwlb_big)
                session.flush()
                session.refresh(zwlb_big)
                zwlb_big_id = zwlb_big.id

            # 职位分类
            zp_zwlb=session.query(Zwlb).filter_by(zwlb_name=item['zwlb']).first()
            if zp_zwlb:
                zwlb_id=zp_zwlb.id
            else:
                zwlb=Zwlb(zwlb_name=item['zwlb'],zwlb_big_id=zwlb_big_id)
                session.add(zwlb)
                session.flush()
                session.refresh(zwlb)
                zwlb_id = zwlb.id
            # 职位大分类
            print(item)
            zp_zwlb_big = session.query(ZwlbBig).filter_by(zwlb_big_name=item['zwlb_big']).first()
            if zp_zwlb_big:
                zwlb_big_id = zp_zwlb_big.id
            else:
                zwlb_big = ZwlbBig(zwlb_big_name=item['zwlb_big'])
                session.add(zwlb_big)
                session.flush()
                session.refresh(zwlb_big)
                zwlb_big_id = zwlb_big.id
            # 公司行业
            zp_gshy = session.query(Gshy).filter_by(gshy_name=item['gshy']).first()
            if zp_gshy:
                gshy_id = zp_gshy.id
            else:
                gshy = Gshy(gshy_name=item['gshy'])
                session.add(gshy)
                session.flush()
                session.refresh(gshy)
                gshy_id = gshy.id
            #公司性质
            zp_gsxz = session.query(Gsxz).filter_by(gsxz_name=item['gsxz']).first()
            if zp_gsxz:
                gsxz_id = zp_gsxz.id
            else:
                gsxz = Gsxz(gsxz_name=item['gsxz'])
                session.add(gsxz)
                session.flush()
                session.refresh(gsxz)
                gsxz_id = gsxz.id

            #工作经验
            zp_gzjy = session.query(Gzjy).filter_by(gzjy_name=item['gzjy']).first()
            if zp_gzjy:
                gzjy_id = zp_gzjy.id
            else:
                gzjy = Gzjy(gzjy_name=item['gzjy'])
                session.add(gzjy)
                session.flush()
                session.refresh(gzjy)
                gzjy_id = gzjy.id
            #学历
            zp_zdxl = session.query(Xl).filter_by(xl_name=item['zdxl']).first()
            if zp_zdxl:
                xl_id = zp_zdxl.id
            else:
                zdxl = Xl(xl_name=item['zdxl'])
                session.add(zdxl)
                session.flush()
                session.refresh(zdxl)
                xl_id = zdxl.id
            #公司规模
            zp_gsgm = session.query(Gsgm).filter_by(gsgm_name=item['gsgm']).first()
            if zp_gsgm:
                gsgm_id = zp_gsgm.id
            else:
                gsgm = Gsgm(gsgm_name=item['gsgm'])
                session.add(gsgm)
                session.flush()
                session.refresh(gsgm)
                gsgm_id = gsgm.id

                
            # 职位列表
            zp_list = session.query(List).filter_by(href=item['href']).first()
            if not zp_list:
                zp_list = List()
                zp_list.zwmc_id = zwmc_id
                zp_list.gsmc_id = gsmc_id
                zp_list.flxx='|'.join(item['flxx'])
                zp_list.min_zwyx=int(item['min_zwyx'])
                zp_list.max_zwyx = int(item['max_zwyx'])
                zp_list.dd_id = dd_id
                zp_list.fbrq=item['fbrq']
                zp_list.gsxz_id=gsxz_id
                zp_list.gzjy_id=gzjy_id
                zp_list.xl_id=xl_id
                if item['zprs']=='若干':
                    item['zprs']=0
                zp_list.zprs=int(item['zprs'])
                zp_list.zwlb_id=zwlb_id
                zp_list.gsgm_id=gsgm_id
                zp_list.gshy_id = gshy_id
                zp_list.gwzz = item['gwzz']
                zp_list.rzyq = item['rzyq']
                zp_list.href = item['href']
                zp_list.zwlb_big_id = zwlb_big_id
                session.add(zp_list)
                session.flush()
                session.refresh(zp_list)


                zp_list_old = ListOld()
                zp_list_old.zwmc = item['zwmc']
                zp_list_old.gsmc = item['gsmc']
                zp_list_old.flxx = '|'.join(item['flxx'])
                zp_list_old.min_zwyx = int(item['min_zwyx'])
                zp_list_old.max_zwyx = int(item['max_zwyx'])
                zp_list_old.dd = item['gzdd']
                zp_list_old.fbrq = item['fbrq']
                zp_list_old.gsxz = item['gsxz']
                zp_list_old.gzjy = item['gzjy']
                zp_list_old.xl = item['zdxl']
                zp_list_old.zprs = int(item['zprs'])
                zp_list_old.zwlb = item['zwlb']
                zp_list_old.gsgm = item['gsgm']
                zp_list_old.gshy = item['gshy']
                zp_list_old.gwzz = item['gwzz']
                zp_list_old.rzyq = item['rzyq']
                zp_list_old.href = item['href']
                zp_list_old.zwlb_big = item['zwlb_big']
                session.add(zp_list_old)
                zp_list_id=zp_list.id

                gwzz_fenci_list = jb_an.extract_tags(item['gwzz'], topK=5)
                rzyq_fenci_list = jb_an.extract_tags(item['rzyq'], topK=5)
                gwzz_parm=[{"fenci": gwzz_item,'list_id':zp_list_id,'zwlb_id':zwlb_id} for gwzz_item in gwzz_fenci_list]
                if gwzz_parm:
                    engine.execute(
                        GwzzFenCi.__table__.insert(),
                        gwzz_parm
                    )
                rzyq_parm=[{"fenci": rzyq_item, 'list_id': zp_list_id, 'zwlb_id': zwlb_id} for rzyq_item in
                     rzyq_fenci_list]
                if rzyq_parm:
                    engine.execute(
                        RzyqFenCi.__table__.insert(),
                        rzyq_parm
                    )
                flxx_parm=[{"flxx_name": flxx_item, 'list_id': zp_list_id, 'zwlb_id': zwlb_id} for flxx_item in item['flxx']]
                if flxx_parm:
                    engine.execute(
                        Flxx.__table__.insert(),
                        flxx_parm
                    )
            session.commit()
        return item