import requests
import pymysql
import json

# 1.查询数据库，获取城市名称
conn=pymysql.connect(host='127.0.0.1',user='root',password='FanTan879',db='zp',charset='utf8')
cursor=conn.cursor()
cursor.execute('select dd_name from zp_dd group by dd_name')
result=cursor.fetchall()
for city in result:
    # 2.拼接查询链接
    if city[0]:
        url='http://apis.map.qq.com/jsapi?qt=poi&wd={}&pn=0&rn=10&rich_source=qipao&rich=web&nj=0&c=1&key=FBOBZ-VODWU-C7SVF-B2BDI-UK3JE-YBFUS&output=jsonp&pf=jsapi&ref=jsapi&cb=qq.maps._svcb3.search_service_0'.format(city)
        # 执行URL查询
        response=requests.get(url).text
        json_text=response.strip('qq.maps._svcb3.search_service_0 && qq.maps._svcb3.search_service_0()')
        #print(json_text)
        json_dict=json.loads(json_text)
        pointx=json_dict['detail']['city'].get('pointx','')
        pointy=json_dict['detail']['city'].get('pointy','')
        cname=json_dict['detail'].get('parent',{}).get('cname','')
        if cname=='中国':
            cname=city[0]
        elif cname=='':
            url='http://api.map.baidu.com/?qt=s&c=289&wd={}&rn=10&ie=utf-8&oue=1&fromproduct=jsapi&res=api&callback=BMap._rd._cbk77164&ak=E4805d16520de693a3fe707cdc962045'.format(city[0]+'市')
            response = requests.get(url).text
            json_text = response.strip('/**/BMap._rd._cbk77164 && BMap._rd._cbk77164()')
            json_dict = json.loads(json_text)
            cname=json_dict['current_city']['up_province_name']

        cname=cname.strip('壮族回族维吾尔市省自治区')
        if cname=='上海':
            cname=city[0]
        # 更新数据库记录
        update_sql='update zp_dd set pointx=%s,pointy=%s,province=%s where dd_name=%s'
        update_parm=(pointx,pointy,cname,city[0])
        print(update_parm)
        cursor.execute(update_sql,update_parm)
        conn.commit()